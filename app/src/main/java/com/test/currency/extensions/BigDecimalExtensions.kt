package com.test.currency.extensions

import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

private const val SCALE = 2

fun BigDecimal.multiplyExact(multiplicand: BigDecimal): BigDecimal = multiply(multiplicand, MathContext.DECIMAL32)

fun BigDecimal.divideExact(divisor: BigDecimal): BigDecimal = divide(divisor, MathContext.DECIMAL32)

fun BigDecimal?.toScaledStringOrEmpty(): String = this?.setScale(SCALE, RoundingMode.HALF_EVEN)
    ?.stripTrailingZeros()
    ?.toPlainString()
    ?: ""
