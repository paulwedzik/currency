package com.test.currency.features.rates.data.model

import java.math.BigDecimal

data class CurrencyRates(
    val baseCurrency: Currency,
    val rates: Map<Currency, BigDecimal>
)
