package com.test.currency.features.rates.presentation

import com.test.currency.base.Event
import com.test.currency.features.rates.domain.model.CurrencyRateModel

data class CurrencyRatesViewState(
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val ratesList: List<CurrencyRateModel> = emptyList(),
    val baseCurrencyChanged: Event<Unit>? = null
)
