package com.test.currency.features.rates.domain.model

import com.test.currency.features.rates.data.model.Currency
import java.math.BigDecimal

data class CurrencyRateModel(
    val rate: BigDecimal = BigDecimal.ONE,
    val amount: String,
    val currency: Currency = Currency.EUR
)
