package com.test.currency.features.rates.presentation

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.test.currency.base.BaseViewModel
import com.test.currency.base.Event
import com.test.currency.base.SchedulersProvider
import com.test.currency.features.rates.domain.CurrencyRatesInteractor
import com.test.currency.features.rates.domain.CurrencyRatesResult
import io.reactivex.Observable
import timber.log.Timber
import javax.inject.Inject
import kotlin.system.exitProcess

class CurrencyRatesViewModel @Inject constructor(
    schedulersProvider: SchedulersProvider,
    private val interactor: CurrencyRatesInteractor
) : BaseViewModel<CurrencyRatesResult, CurrencyRatesViewState, CurrencyRatesIntent>(schedulersProvider),
    LifecycleObserver {

    override fun handleIntent(intent: CurrencyRatesIntent): Observable<CurrencyRatesResult> = when (intent) {
        CurrencyRatesIntent.GetCurrencyRates -> interactor.getCurrencyRatesUpdates()
        is CurrencyRatesIntent.AmountChanged -> interactor.updateList(
            intent.changedAmount,
            viewState.value?.ratesList
        )
        is CurrencyRatesIntent.BaseCurrencyChanged -> {
            val listCopy = viewState.value?.ratesList?.toMutableList()
            listCopy?.removeAt(intent.position)?.also {
                listCopy.add(0, it)
            }
            interactor.currentList = listCopy
            if (listCopy != null) {
                Observable.just<CurrencyRatesResult>(CurrencyRatesResult.OrderChanged(listCopy.toList()))
            } else {
                Observable.empty<CurrencyRatesResult>()
            }
        }
    }

    override fun reduceViewState(result: CurrencyRatesResult): CurrencyRatesViewState = when (result) {
        CurrencyRatesResult.Loading -> CurrencyRatesViewState(isLoading = true)
        CurrencyRatesResult.Error -> CurrencyRatesViewState(isError = true)
        is CurrencyRatesResult.Rates -> CurrencyRatesViewState(ratesList = result.ratesList)
        is CurrencyRatesResult.OrderChanged -> CurrencyRatesViewState(ratesList = result.ratesList, baseCurrencyChanged = Event(Unit))
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onLifecycleResume() {
        interactor.resumeUpdates()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onLifecyclePause() {
        interactor.pauseUpdates()
    }
}
