package com.test.currency.features.rates.domain

import com.test.currency.features.rates.domain.model.CurrencyRateModel

sealed class CurrencyRatesResult {
    object Loading : CurrencyRatesResult()
    object Error : CurrencyRatesResult()
    data class Rates(val ratesList: List<CurrencyRateModel>) : CurrencyRatesResult()
    data class OrderChanged(val ratesList: List<CurrencyRateModel>) : CurrencyRatesResult()
}
