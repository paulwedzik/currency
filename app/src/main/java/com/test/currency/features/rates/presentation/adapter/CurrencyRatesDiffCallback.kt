package com.test.currency.features.rates.presentation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.test.currency.features.rates.domain.model.CurrencyRateModel

class CurrencyRatesDiffCallback(
    private val oldList: List<CurrencyRateModel>,
    private val newList: List<CurrencyRateModel>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = true
//        oldList[oldItemPosition].currency.name === newList[newItemPosition].currency.name

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] === newList[newItemPosition]

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size
}
