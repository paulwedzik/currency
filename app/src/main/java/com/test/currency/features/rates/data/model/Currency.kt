package com.test.currency.features.rates.data.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.test.currency.R

/**
 * Used by Gson
 */
//TODO Use java.util Currency
@Suppress("unused")
enum class Currency(
    @StringRes val currencyName: Int,
    @DrawableRes val icon: Int
) {

    EUR(R.string.currencyEURName, R.drawable.ic_eu),
    USD(R.string.currencyUSDName, R.drawable.ic_us),
    AUD(R.string.currencySEKName, R.drawable.ic_sweden),
    BGN(R.string.currencySEKName, R.drawable.ic_sweden),
    BRL(R.string.currencySEKName, R.drawable.ic_sweden),
    CHF(R.string.currencySEKName, R.drawable.ic_sweden),
    CNY(R.string.currencySEKName, R.drawable.ic_sweden),
    CZK(R.string.currencySEKName, R.drawable.ic_sweden),
    DKK(R.string.currencySEKName, R.drawable.ic_sweden),
    GBP(R.string.currencySEKName, R.drawable.ic_sweden),
    HKD(R.string.currencySEKName, R.drawable.ic_sweden),
    HRK(R.string.currencySEKName, R.drawable.ic_sweden),
    HUF(R.string.currencySEKName, R.drawable.ic_sweden),
    IDR(R.string.currencySEKName, R.drawable.ic_sweden),
    ILS(R.string.currencySEKName, R.drawable.ic_sweden),
    INR(R.string.currencySEKName, R.drawable.ic_sweden),
    ISK(R.string.currencySEKName, R.drawable.ic_sweden),
    KRW(R.string.currencySEKName, R.drawable.ic_sweden),
    MXN(R.string.currencySEKName, R.drawable.ic_sweden),
    MYR(R.string.currencySEKName, R.drawable.ic_sweden),
    NOK(R.string.currencySEKName, R.drawable.ic_sweden),
    NZD(R.string.currencySEKName, R.drawable.ic_sweden),
    PHP(R.string.currencySEKName, R.drawable.ic_sweden),
    PLN(R.string.currencySEKName, R.drawable.ic_sweden),
    RON(R.string.currencySEKName, R.drawable.ic_sweden),
    SEK(R.string.currencySEKName, R.drawable.ic_sweden),
    SGD(R.string.currencySEKName, R.drawable.ic_sweden),
    THB(R.string.currencySEKName, R.drawable.ic_sweden),
    RUB(R.string.currencySEKName, R.drawable.ic_sweden),
    JPY(R.string.currencySEKName, R.drawable.ic_sweden),
    ZAR(R.string.currencySEKName, R.drawable.ic_sweden),
    CAD(R.string.currencyCADName, R.drawable.ic_canada);
}
