package com.test.currency.features.rates.presentation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import com.test.currency.R
import com.test.currency.features.rates.domain.model.CurrencyRateModel
import com.test.currency.features.rates.presentation.CurrencyRatesIntent.*
import com.test.currency.features.rates.presentation.adapter.CurrencyRatesAdapter
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_currencies.*
import timber.log.Timber
import javax.inject.Inject

class CurrencyRatesActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val ratesAdapter = CurrencyRatesAdapter {
        viewModel.dispatchIntent(it)
    }

    private lateinit var viewModel: CurrencyRatesViewModel

    override fun androidInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currencies)
        AndroidInjection.inject(this)
        initRecyclerView()
        initViewModel()
    }

    private fun initRecyclerView() {
        recyclerViewCurrencies.apply {
            adapter = ratesAdapter
            (itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory)[CurrencyRatesViewModel::class.java]
        lifecycle.addObserver(viewModel)
        viewModel.viewState.observe(this, Observer { viewState ->
            renderState(viewState)
        })
        viewModel.dispatchInitialIntent(GetCurrencyRates)
    }

    private fun renderState(viewState: CurrencyRatesViewState) {
        Timber.d("~!@ View state: $viewState")
        when {
            viewState.isLoading -> showLoading()
            viewState.isError -> showError()
            viewState.baseCurrencyChanged?.getContentIfNotHandled()!= null -> ratesAdapter.notifyItemMoved(1, 0)
            viewState.ratesList.isNotEmpty() -> showNews(viewState.ratesList)
        }
    }

    private fun showLoading() {
        recyclerViewCurrencies.visibility = View.GONE
        textViewError.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    private fun showError() {
        progressBar.visibility = View.GONE
        textViewError.visibility = View.VISIBLE
    }

    private fun showNews(ratesList: List<CurrencyRateModel>) {
        Timber.d("~~!@ showlist: ")
        progressBar.visibility = View.GONE
        recyclerViewCurrencies.visibility = View.VISIBLE
        ratesAdapter.update(ratesList)
    }
}
