package com.test.currency.features.rates.domain

import com.test.currency.features.rates.data.repository.CurrencyRatesRepository
import com.test.currency.features.rates.domain.model.CurrencyRateModel
import com.test.currency.features.rates.domain.model.CurrencyRatesModelMapper
import io.reactivex.Observable
import timber.log.Timber
import javax.inject.Inject

class CurrencyRatesInteractor @Inject constructor(
    private val repository: CurrencyRatesRepository,
    private val mapper: CurrencyRatesModelMapper
) {

    var currentList: List<CurrencyRateModel>? = null

    fun getCurrencyRatesUpdates(): Observable<CurrencyRatesResult> =
        repository.getCurrencyRatesUpdates()
            .map<CurrencyRatesResult> { CurrencyRatesResult.Rates(mapper.map(currentList, it)) }
            .onErrorReturn {
                Timber.e(it)
                CurrencyRatesResult.Error
            }
            .startWith(CurrencyRatesResult.Loading)

//    fun updateBaseCurrency(currencyRates: List<CurrencyRateModel>): Observable<CurrencyRatesResult> {
//        val list = currencyRates.let { mapper.map(currencyRates) }
//        currentList = list
//        return Observable.just(CurrencyRatesResult.Rates(list))
//    }

    fun updateList(
        newAmount: String,
        ratesList: List<CurrencyRateModel>?
    ): Observable<CurrencyRatesResult> {
        val list = ratesList?.let { mapper.mapUpdateAmount(newAmount, it) } ?: emptyList<CurrencyRateModel>()
        currentList = list
        return Observable.just(CurrencyRatesResult.Rates(list))
    }

    fun resumeUpdates() {
        repository.resume()
    }

    fun pauseUpdates() {
        repository.pause()
    }

    companion object {
        private const val INITIAL_AMOUNT = "100"
    }
}
