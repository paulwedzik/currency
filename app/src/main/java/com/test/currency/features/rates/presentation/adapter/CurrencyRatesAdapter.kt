package com.test.currency.features.rates.presentation.adapter

import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.currency.R
import com.test.currency.extensions.showKeyboard
import com.test.currency.features.rates.domain.model.CurrencyRateModel
import com.test.currency.features.rates.presentation.CurrencyRatesIntent
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_currency_rate.*

class CurrencyRatesAdapter(
    private val ratesIntentCallback: (CurrencyRatesIntent) -> Unit
) : RecyclerView.Adapter<CurrencyRatesAdapter.ViewHolder>() {

    private var currencyRates: MutableList<CurrencyRateModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_currency_rate, parent, false)
        ).apply {
            initClickListener(parent)
            initEditText()
        }

    private fun ViewHolder.initClickListener(parent: ViewGroup) {
        itemView.setOnClickListener {
            val recyclerView = (parent as RecyclerView)
            moveItemToTopOfList(adapterPosition, recyclerView.layoutManager)
            showKeyboard(editTextAmount)
            resetTextWatcher()
        }
    }

    private fun ViewHolder.initEditText() {
        editTextAmount.apply {
            setOnTouchListener { view, motionEvent ->
                (view.parent as View).onTouchEvent(motionEvent)
            }
        }
    }

    private fun showKeyboard(editText: EditText) {
        editText.apply {
            requestFocus()
            showKeyboard()
            setSelection(text.length)
        }
    }

    private fun moveItemToTopOfList(position: Int, layoutManager: RecyclerView.LayoutManager?) {
        if (position != 0) {
//            currencyRates[0].copy()
//            currencyRates.removeAt(position).also {
//                currencyRates.add(0, it)
//                ratesIntentCallback(CurrencyRatesIntent.BaseCurrencyChanged(position))
//            }
            ratesIntentCallback(CurrencyRatesIntent.BaseCurrencyChanged(position))
//            notifyItemMoved(position, 0)
//            layoutManager?.scrollToPosition(0)
        }
    }

    override fun getItemCount() = currencyRates.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currencyRates[position])
    }

    fun update(ratesList: List<CurrencyRateModel>) {
        val diffCallback = CurrencyRatesDiffCallback(oldList = currencyRates, newList = ratesList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        currencyRates = ratesList.toMutableList()
        diffResult.dispatchUpdatesTo(this)
    }

    // TODO extract to separa
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), LayoutContainer {
        fun bind(rate: CurrencyRateModel) {
            imageViewFlag.setImageResource(rate.currency.icon)
            textViewCurrency.text = rate.currency.name
            textViewCurrencyName.setText(rate.currency.currencyName)
            editTextAmount.setText(rate.amount)
            editTextAmount.setSelection(rate.amount.length)
            resetTextWatcher()
        }

        // TODO refactor that code
        fun resetTextWatcher() {
            if (adapterPosition == 0) {
                editTextAmount.setOnKeyListener { v, keyCode, event ->
                    when (event.action) {
                        KeyEvent.ACTION_DOWN -> {
                        }
                        KeyEvent.ACTION_UP -> {
                            val textAmount = editTextAmount.text.toString()
                            ratesIntentCallback(CurrencyRatesIntent.AmountChanged(textAmount))
                        }
                    }
                    false
                }
            }
        }

        override val containerView: View?
            get() = itemView
    }
}

