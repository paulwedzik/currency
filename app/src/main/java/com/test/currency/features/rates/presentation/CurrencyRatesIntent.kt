package com.test.currency.features.rates.presentation

sealed class CurrencyRatesIntent {
    object GetCurrencyRates : CurrencyRatesIntent()

    data class BaseCurrencyChanged(val position: Int) : CurrencyRatesIntent()
    data class AmountChanged(val changedAmount: String) : CurrencyRatesIntent()
}
