package com.test.currency.features.rates.data.repository

import com.test.currency.features.rates.data.model.CurrencyRates
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

class CurrencyRatesRepository @Inject constructor(private val api: CurrencyRatesApi) {

    private var isResumed = AtomicBoolean(true)

    // TODO check with low network and network errors
    fun getCurrencyRatesUpdates(): Observable<CurrencyRates> =
        // TODO hardcoded String
        api.fetchCurrencyRates("EUR")
//            .repeatWhen { Observable.interval(1, TimeUnit.SECONDS, Schedulers.io()).filter { isResumed.get() } }
            .distinctUntilChanged()

    fun resume() {
        isResumed.set(true)
    }

    fun pause() {
        isResumed.set(false)
    }
}
