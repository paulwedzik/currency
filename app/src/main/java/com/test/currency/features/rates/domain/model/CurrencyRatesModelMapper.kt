package com.test.currency.features.rates.domain.model

import com.test.currency.extensions.divideExact
import com.test.currency.extensions.multiplyExact
import com.test.currency.extensions.toScaledStringOrEmpty
import com.test.currency.features.rates.data.model.Currency
import com.test.currency.features.rates.data.model.CurrencyRates
import timber.log.Timber
import java.math.BigDecimal
import javax.inject.Inject

class CurrencyRatesModelMapper @Inject constructor() {

    fun map(currentList: List<CurrencyRateModel>?, currencyRates: CurrencyRates) =
        mutableListOf<CurrencyRateModel>().apply {
            val baseCurrency = currentList?.first() ?: CurrencyRateModel(amount = "100")
            val tmpBaseRate = currencyRates.rates[baseCurrency.currency] ?: BigDecimal.ONE
            baseCurrency.copy(rate = BigDecimal.ONE).also { add(it) }
            if (currentList != null) {
                currentList.drop(1).forEach { rateModel ->
                    val rate = currencyRates.rates[rateModel.currency]
                    val newRate = rate?.divideExact(tmpBaseRate) ?: BigDecimal.ONE.divideExact(tmpBaseRate)
                    add(createRateModel(baseCurrency.amount.toBigDecimalOrNull(), newRate, rateModel.currency))
                }
            } else {
                currencyRates.rates.forEach { (currency, rate) ->
                    add(createRateModel(baseCurrency.amount.toBigDecimalOrNull(), rate, currency))
                }
            }
        }

    private fun createRateModel(baseAmount: BigDecimal?, rate: BigDecimal, currency: Currency): CurrencyRateModel {
        val calculatedAmount = baseAmount?.let { rate.multiplyExact(it) }
        return CurrencyRateModel(rate, calculatedAmount.toScaledStringOrEmpty(), currency)
    }

    fun mapUpdateAmount(newAmount: String, currencyRates: List<CurrencyRateModel>) =
        mutableListOf<CurrencyRateModel>().apply {
            val baseCurrency = currencyRates.first()
            Timber.d("~!@ baseCurrency: $baseCurrency")
            val newCurrency = baseCurrency.copy(amount = newAmount)
            Timber.d("~!@ newCurrency: $newCurrency")
            add(newCurrency)
            currencyRates.drop(1).forEach { rateModel ->
                val rate = if (rateModel.rate == BigDecimal.ONE) {
                    BigDecimal.ONE.divideExact(baseCurrency.rate)
                } else {
                    rateModel.rate
                }
                val model =
                    createRateModel(newCurrency.amount.toBigDecimalOrNull(), rate, rateModel.currency)
                Timber.d("~!@ changedModel: $model")
                add(model)
            }
        }
}
