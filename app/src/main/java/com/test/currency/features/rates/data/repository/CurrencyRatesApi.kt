package com.test.currency.features.rates.data.repository

import com.test.currency.features.rates.data.model.CurrencyRates
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyRatesApi {

    @GET("latest")
    fun fetchCurrencyRates(@Query("base") baseCurrency: String): Observable<CurrencyRates>
}
