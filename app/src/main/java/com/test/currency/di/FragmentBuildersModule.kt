package com.test.currency.di

import dagger.Module

/**
 * Module for contributing fragments
 */
@Suppress("unused")
@Module
abstract class FragmentBuildersModule
