package com.test.currency.di

import com.test.currency.features.rates.presentation.CurrencyRatesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module for contributing app activities
 */
@Suppress("unused")
@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeCurrenciesActivity(): CurrencyRatesActivity
}
